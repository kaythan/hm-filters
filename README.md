# About
This is just a small project to add filters to the side of a page.

# Files

## index.html
Contains the basic html for the side bar & original tableauPlaceholder div

## style.css
Some pretty styles

## script.js
All the javascript


