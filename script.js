const STORAGE_NAMES = {
    FACILITY_FILTER: "FACILITY_FILTER",
    OTHER_FILTERS: "OTHER_FILTERS",
};

//---------------------------------
//          HTML Generators
//---------------------------------

function createLabeledInput(id, description, inputName, isSelected) {
    let checkboxInput = document.createElement("input");
    checkboxInput.setAttribute("type", "checkbox");
    checkboxInput.setAttribute("id", id);
    checkboxInput.setAttribute("name", inputName);
    checkboxInput.setAttribute("value", id);
    if (isSelected) {
        checkboxInput.setAttribute("checked", "true");
    }

    let facilityLabel = document.createElement("label");
    facilityLabel.setAttribute("for", id);
    facilityLabel.appendChild(checkboxInput);
    facilityLabel.appendChild(document.createTextNode(description));

    return facilityLabel;
}

function createFilter(id, title, options, preselected) {
    let titleTextDiv = document.createElement("div");
    titleTextDiv.appendChild(document.createTextNode(title));

    let collapseIndicator = document.createElement("div");
    collapseIndicator.setAttribute("class", "collapse-indicator");
    collapseIndicator.appendChild(document.createTextNode("+"));

    let titleDiv = document.createElement("div");
    titleDiv.setAttribute("class", "filter-title");
    titleDiv.setAttribute("onclick", "filterTitleClick(event)");
    titleDiv.appendChild(titleTextDiv);
    titleDiv.appendChild(collapseIndicator);

    let optionsDiv = document.createElement("div");
    optionsDiv.setAttribute("id", id);
    optionsDiv.setAttribute("class", "filter-content collapsed");

    options.forEach((o) => {
        let isSelected = preselected !== null && preselected.includes(o.id);
        let labeledInput = createLabeledInput(o.id, o.description, id, isSelected);
        optionsDiv.appendChild(labeledInput);
    });

    let filterDiv = document.createElement("div");
    filterDiv.setAttribute("class", "filter");
    filterDiv.appendChild(titleDiv);
    filterDiv.appendChild(optionsDiv);
    return filterDiv;
}

//-----------------------------------------
//           Initialization code
//-----------------------------------------

function docReady() {
    //-------------------------------------
    // TODO: Megan: Fetch facilities and format like this list.
    //-------------------------------------
    let facilities = [
        { id: "facility0", description: "Facility 0" },
        { id: "facility1", description: "Facility 1" },
        { id: "facility2", description: "Facility 2" },
        { id: "facility3", description: "Facility 3" },
        { id: "facility4", description: "Facility 4" },
    ];

    // Load previous selections from local storage
    let preselectedFacilities = JSON.parse(window.localStorage.getItem(STORAGE_NAMES.FACILITY_FILTER));

    // Insert label/input elements
    let facilityFilter = document.getElementById("facility-filter");
    facilities.reverse().forEach((f) => {
        let isSelected = preselectedFacilities !== null && preselectedFacilities.includes(f.id);
        let labeledInput = createLabeledInput(f.id, f.description, "facility", isSelected);
        facilityFilter.insertBefore(labeledInput, facilityFilter.firstChild);
    });

    //-------------------------------------
    // TODO: Megan: Fetch other filters and format like this list
    //-------------------------------------
    let otherFilters = [
        {
            id: "animalFilter",
            title: "Animal Filter",
            options: [
                { id: "animal0", description: "Weasel" },
                { id: "animal1", description: "Stoat" },
                { id: "animal2", description: "Ermine" },
            ],
        },
        {
            id: "fruitFilter",
            title: "Fruit Filter",
            options: [
                { id: "fruit0", description: "Banana" },
                { id: "fruit1", description: "Pineapple" },
                { id: "fruit2", description: "Grapefruit" },
            ],
        },
        {
            id: "colorFilter",
            title: "Color Filter",
            options: [
                { id: "color0", description: "Aquamarine" },
                { id: "color1", description: "Chartreuse" },
                { id: "color2", description: "Magenta" },
            ],
        },
    ];

    let otherFiltersDiv = document.getElementById("other-filters");
    let preselectedOtherFilters = [];
    otherFilters.forEach((f) => {
        // Load previous selections from local storage
        let preselected = JSON.parse(window.localStorage.getItem(STORAGE_NAMES.OTHER_FILTERS + "." + f.id));
        if (preselected) {
            preselectedOtherFilters.push({
                filterId: f.id,
                selectedValues: preselected,
            });
        }
        let filterDiv = createFilter(f.id, f.title, f.options, preselected);
        otherFiltersDiv.appendChild(filterDiv);
    });

    // Upon initial load, we need to apply the preselected filters! This
    // might not be the right place to call these functions.
    applyFacilitiesFilter(preselectedFacilities);
    applyOtherFilters(preselectedOtherFilters);
}

// --------------------------------
// Action Handlers (buttons and stuff)
// --------------------------------

function filterTitleClick(e) {
    let title = e.currentTarget;
    let plusMinus = title.children.item(1);
    let content = title.parentNode.children.item(1);

    if (content.classList.contains("collapsed")) {
        plusMinus.innerHTML = "-";
        content.classList.remove("collapsed");
    } else {
        plusMinus.innerHTML = "+";
        content.classList.add("collapsed");
    }
}

function filterFacilitiesClick(e) {
    // Gather selected facilities
    let selectedFacilityIds = getAllValuesForCheckedInputsWithin(document.getElementById("facility-filter"));

    // Store in localStorage
    window.localStorage.setItem(STORAGE_NAMES.FACILITY_FILTER, JSON.stringify(selectedFacilityIds));

    applyFacilitiesFilter(selectedFacilityIds);
}

function filterOthersClick(e) {
    // Gather all checkbox information
    let otherFiltersDiv = document.getElementById("other-filters");
    let inputGroupDivs = otherFiltersDiv.querySelectorAll(".filter-content");
    let selectedInputGroups = Array.from(inputGroupDivs).map((g) => ({
        filterId: g.id,
        selectedValues: getAllValuesForCheckedInputsWithin(g),
    }));

    // Store in localStorage
    selectedInputGroups.forEach((sig) => {
        window.localStorage.setItem(
            STORAGE_NAMES.OTHER_FILTERS + "." + sig.filterId,
            JSON.stringify(sig.selectedValues)
        );
    });

    applyOtherFilters(selectedInputGroups);
}

//-----------------------------------------
//               Utilities
//-----------------------------------------

// Returns a list of strings of input ids where input.checked == true
function getAllValuesForCheckedInputsWithin(parent) {
    return Array.from(parent.querySelectorAll("input"))
        .filter((i) => i.checked)
        .map((i) => i.id);
}

function applyFacilitiesFilter(selectedFacilityIds) {
    //-------------------------------------
    // TODO: Megan: Here use the Tableau API to apply the facility filter!
    // the selectedFacilityIds will have an array of strings which represent
    // the ids of the facilities that have been selected by the user.
    //-------------------------------------

    // The following code should be removed or commented in real life!
    if (selectedFacilityIds && selectedFacilityIds.length > 0) {
        let message = "These facilities were selected:\n";
        message += selectedFacilityIds.join("\n");
        alert(message);
    } else {
        alert("No facilities checked!");
    }
}

function applyOtherFilters(selectedInputGroups) {
    //-------------------------------------
    // TODO: Megan: Here use the Tableau API to apply all the "other" filters!
    // the selectedInputGroups variable will have an array of objects where each
    // object constists of filterId (the identifier for the filter itself, aka how
    // you can tell one filter from any other) and selectedValues (a list of
    // ids of the items which have been selected by the user)
    //-------------------------------------

    // The following code should be removed or commented in real life!
    if (selectedInputGroups && selectedInputGroups.length > 0) {
        let message = selectedInputGroups
            .map((g) => {
                if (g.selectedValues.length == 0) {
                    return g.filterId + ": None";
                } else {
                    return g.filterId + ": " + g.selectedValues.join(", ");
                }
            })
            .join("\n");
        alert(message);
    } else {
        alert('Hmm, no filters in "other filters" section');
    }
}

document.body.onload = docReady;
